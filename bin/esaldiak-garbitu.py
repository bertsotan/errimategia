#!/usr/bin/env python3

###############################################################################
#
# Script honek deskargatutako esaldiak pixka bat garbitu eta lehen hitza
# kentzen die. Lehen hitzak beti hasten dira letra larriz eta deskartatu behar
# dira.
#
# Sarrerak eta irteerak:
#    <-- input/validated_sentences_cv-corpus-18.0.tsv
#    --> input-output/esaldiak-garbi
#    --> (pantaila)
#
###############################################################################

import datetime
import csv

# Fitxategiak
inputEsaldiakDownloadTsvBidea = "../input/validated_sentences_cv-corpus-18.0.tsv"
ouputEsaldiakGarbiBidea = "../input-output/esaldiak-garbi"

hasiDataOrdua = datetime.datetime.now()
print("HASI - " + str(hasiDataOrdua))

###############################################################################

print("\nDeskargatutako esaldiak garbitzen (validated_sentences.tsv)...")

ouputEsaldiakGarbiFitxategia = open(ouputEsaldiakGarbiBidea, 'w')

esaldiKopurua = 0

with open(inputEsaldiakDownloadTsvBidea) as inputEsaldiakDownloadTsvFitxategia:
    esaldiakTsvFitxategia = csv.reader(inputEsaldiakDownloadTsvFitxategia, delimiter="\t")

    for lerroa in esaldiakTsvFitxategia:

        # Lerrorik ez badago, buklea amaitu
        if not lerroa:
            break

        # Esaldia bigarren balorea da
        esaldia = lerroa[1]

        # Lerro guztian zehar karaktere hauek kendu

        esaldia = esaldia.replace('...', '')
        esaldia = esaldia.replace(',', '')
        esaldia = esaldia.replace(':', '')
        esaldia = esaldia.replace(';', '')
        esaldia = esaldia.replace('…', '')
        esaldia = esaldia.replace('?', '')
        esaldia = esaldia.replace('‘', '')
        esaldia = esaldia.replace('’', '')
        esaldia = esaldia.replace('“', '')
        esaldia = esaldia.replace('”', '')
        esaldia = esaldia.replace('«', '')
        esaldia = esaldia.replace('»', '')
        esaldia = esaldia.replace('"', '')
        esaldia = esaldia.replace(chr(194), '')  # Â kendu
        esaldia = esaldia.replace(chr(173), '')  # Ikusten ez den karaktere arraroa
        esaldia = esaldia.replace(chr(8203), '')  # Ikusten ez den karaktere arraroa
        esaldia = esaldia.replace('\'', '')  # Ikusten ez den karaktere arraroa

        # Lerro guztian zehar karaktere hauek ordezkatu
        esaldia = esaldia.replace('!', ' ')
        esaldia = esaldia.replace("\u2013", '-')  # Gidoia normalagatik ordezkatu
        esaldia = esaldia.replace("\u2011", '-')  # Gidoia normalagatik ordezkatu
        esaldia = esaldia.replace("\u2010", '-')  # Gidoia normalagatik ordezkatu
        esaldia = esaldia.replace("—", '-')  # Gidoia normalagatik ordezkatu

        # Puntuazio hutsune bereziak hutsune arrunt bihurtu
        esaldia = esaldia.replace(chr(8200), ' ')
        esaldia = esaldia.replace(' ', ' ')
        esaldia = esaldia.replace(' ', ' ')

        # Bukaeran punturik badago, bukaerako puntu hori kendu
        if esaldia.endswith('.'):
            esaldia = esaldia.rstrip('.')

        # Lerroan oraindik punturen bat geratzen bada, lerro honekin ez jarraitu
        if esaldia.find('.') < 1:
            # Gutxienez bi hitz ez baditu, lerro honekin ez jarraitu
            hitzak = esaldia.split(' ', 1)
            if len(hitzak) >= 2:
                # Bigarren hitzetik aurrerakoa gorde
                lerroaBigarrenetik = hitzak[1]

                # Ezker zein eskuineko hutsunerik baleko, kendu
                lerroaBigarrenetik = lerroaBigarrenetik.strip()

                ouputEsaldiakGarbiFitxategia.write(lerroaBigarrenetik + "\n")
                esaldiKopurua += 1

ouputEsaldiakGarbiFitxategia.close()

print(
    "\tDeskargatutako esaldiak: " +
    str(esaldiKopurua)
)

###############################################################################

amaituDataOrdua = datetime.datetime.now()
print(
    "\nAMAITU - " + str(amaituDataOrdua) +
    " (" + str(amaituDataOrdua - hasiDataOrdua) + ")"
)
