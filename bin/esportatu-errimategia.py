#!/usr/bin/env python3

###############################################################################
#
# Script honek errimategia JSON formatuan gordetzen du eta bere edukiari
# buruzko estatistika txiki batzuk ere gordetzen ditu.
#
# Besteak beste ondokoak egiten ditu:
#   - Gai desberdinak zerrendatu.
#   - Errimategia JSON formatuan gorde.
#   - Edukiari buruzko adierazle batzuk pantailaratzen ditu eta fitxategi
#     batean ere gordetzen ditu.
#
# Sarrerak eta irteerak:
#     <-- input-output/errimategia
#     --> output/errimategia.json
#     --> output/errimategia-info
#     --> (pantaila)
#
###############################################################################

import datetime
import re
import sys
import json
import shutil
import os
from lib import (
    lortu_proiektuaren_bidea,
    lortu_ahoskera1,
    lortu_ahoskera2,
    lortu_azken3ak,
    lortu_bukaera1,
    lortu_bukaera2,
    lortu_hitza_lerrotik,
    lortu_lerroa_hitzetik,
    bilatu_hitza_zerrendan
)

# errimategia.json fitxategiaren formatuaren bertsioa (ez edukiarena)
errimategiarenFormatua = "1.0"

# Fitxategiak
proiektuarenBidea = lortu_proiektuaren_bidea()
inputErrimategiaBidea = proiektuarenBidea + "/input-output/errimategia"
outputErrimategiaJsonBidea = proiektuarenBidea + "/output/errimategia.json"
outputErrimategiaJsonBabeskopiaBidea = outputErrimategiaJsonBidea + ".back"
outputEstatistikakBidea = proiektuarenBidea + "/output/errimategia-info"

# Estatistikak
polisemikoKopurua = 0
lokalKopurua = 0
lagunartekoKopurua = 0
erderakadaKopurua = 0
deklinatuKopurua = 0

###############################################################################

hasiDataOrdua = datetime.datetime.now()
print("HASI - " + str(hasiDataOrdua))

###############################################################################

print("\nErrimategia:")

print("\n\tAurkitutako gai desberdinak zerrendatzen...")

errimategikoGaiak = list()

inputErrimategiaFitxategia = open(inputErrimategiaBidea, 'r')

while True:
    lerroa = inputErrimategiaFitxategia.readline()

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    if lerroa and lerroa[0] != "#":
        lerrokoGaiak = re.findall(r'\{([^}]*)\}', lerroa)

        for lerrokoGaia in lerrokoGaiak:
            if lerrokoGaia not in errimategikoGaiak:
                errimategikoGaiak.append(lerrokoGaia)

errimategikoGaiak.sort()

for errimategikoGaia in errimategikoGaiak:
    print("\t\t - " + errimategikoGaia)

###############################################################################

print("\n\tErrimategiko errimen zerrenda lortzen...")

errimategikoHitzak = list()

inputErrimategiaFitxategia.seek(0)

while True:
    lerroa = inputErrimategiaFitxategia.readline()

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    if len(lerroa) == 0:
        continue

    # Lerro osoa iruzkina bada, saltatu lerroa
    if lerroa[0] == "#":
        continue

    # print ("\tLerroa: " + lerroa)

    hitza, estatistikak = lortu_hitza_lerrotik(lerroa)

    if len(hitza["ahoskera1"]) > 2:  # "hi" bezalako hitzak ezikusi
        polisemikoKopurua += estatistikak["polisemikoak"]
        lokalKopurua += estatistikak["lokalak"]
        lagunartekoKopurua += estatistikak["lagunartekoak"]
        erderakadaKopurua += estatistikak["erderakadak"]
        deklinatuKopurua += estatistikak["deklinatuak"]

        errimategikoHitzak.append(hitza)

print("\t\tAurkitutako hitzak: " + str(len(errimategikoHitzak)))

inputErrimategiaFitxategia.close()

###############################################################################

print("\n\tErrimategiko bukaeren zerrenda lortzen...")

errimategikoBukaerak = list()

for hitza in errimategikoHitzak:
    bukaeraGehitu = True
    for errimaBukaera in errimategikoBukaerak:
        if errimaBukaera["ahoskera1"] == hitza["bukaera"]:
            bukaeraGehitu = False
            break
    if bukaeraGehitu:
        ahoskera2 = lortu_bukaera2(hitza["bukaera"])
        errimaBukaeraBerria = dict()
        errimaBukaeraBerria["ahoskera1"] = hitza["bukaera"]
        errimaBukaeraBerria["ahoskera2"] = ahoskera2
        # print(errimaBukaeraBerria)
        errimategikoBukaerak.append(errimaBukaeraBerria)

errimategikoBukaerak = sorted(
    errimategikoBukaerak,
    key=lambda errimaBukaera: errimaBukaera["ahoskera1"]
)

print("\t\tAurkitutako bukaerak: " + str(len(errimategikoBukaerak)))

###############################################################################

print(
    "\nFitxategi baten babeskopia egiten...\n" +
    "\tJatorria: " + outputErrimategiaJsonBidea + "\n" +
    "\tKopia:    " + outputErrimategiaJsonBabeskopiaBidea
)

egungoBideOsoa = os.getcwd()

shutil.copy(
    outputErrimategiaJsonBidea,
    outputErrimategiaJsonBabeskopiaBidea
)

###############################################################################

print(
    "\nErrimategia JSON formatuan gordetzen...\n\t" +
    outputErrimategiaJsonBidea
)

errimategia = dict()

# Bertsioa
errimategia["formatua"] = errimategiarenFormatua

# Data
# print(str(datetime.datetime.now())[:10])
errimategiData = datetime.datetime.now().strftime("%Y-%m-%d")
errimategia["data"] = errimategiData

# Gaiak
errimategia["gaiak"] = errimategikoGaiak

# Bukaerak
errimategia["bukaerak"] = errimategikoBukaerak

# Hitzak
# JSON formatuan nahi ez ditugun gakoak: ahoskeraEskuzkoaDa
errimategikoHitzakJsonerako = [{key: val for key, val in sub.items() if (
    key != "ahoskeraEskuzkoaDa")}
    for sub in errimategikoHitzak]
errimategia["hitzak"] = errimategikoHitzakJsonerako

outputErrimategiaJson = open(outputErrimategiaJsonBidea, 'w')

outputErrimategiaJson.write(json.dumps(errimategia, indent=2))

outputErrimategiaJson.close()

###############################################################################

outputEstatistikak = open(outputEstatistikakBidea, 'w')

estatistikak = list()

estatistikak.append("Errimategiaren estatistikak:")

estatistikak.append("\tData:\t\t\t\t" + errimategiData)

estatistikak.append("\tGai kopurua:\t\t" + str(len(errimategikoGaiak)))

estatistikak.append("\tBukaera kopurua:\t" + str(len(errimategikoBukaerak)))

estatistikak.append("\tHitz kopurua:\t\t" + str(len(errimategikoHitzak)))

estatistikak.append(
    "\t\tPolisemikoak:\t" + str(polisemikoKopurua) + "\t\t(% " +
    str(round(polisemikoKopurua/len(errimategikoHitzak)*100, 2)) + ")"
)
estatistikak.append(
    "\t\tLokalak:\t\t" + str(lokalKopurua) +
    "\t\t(% " + str(round(lokalKopurua/len(errimategikoHitzak)*100, 2)) + ")"
)
estatistikak.append(
    "\t\tLagunartekoak:\t" + str(lagunartekoKopurua) +
    "\t\t(% " +
    str(round(lagunartekoKopurua/len(errimategikoHitzak)*100, 2)) + ")"
)
estatistikak.append(
    "\t\tErderakadak:\t" + str(erderakadaKopurua) +
    "\t\t(% " + str(round(erderakadaKopurua/len(errimategikoHitzak)*100, 2)) +
    ")"
)
estatistikak.append(
    "\t\tDeklinatuak:\t" + str(deklinatuKopurua) +
    "\t(% " + str(round(deklinatuKopurua/len(errimategikoHitzak)*100, 2)) +
    ")"
)

print("\n")

for idatzi in estatistikak:
    print(idatzi)

print(
    "\nEstatistikak fitxategian gordetzen...\n\t" +
    outputEstatistikakBidea
)

for idatzi in estatistikak:
    outputEstatistikak.write(idatzi + "\n")

outputEstatistikak.close()

###############################################################################

amaituDataOrdua = datetime.datetime.now()
print(
    "\nAMAITU - " + str(amaituDataOrdua) +
    " (" + str(amaituDataOrdua - hasiDataOrdua) + ")"
)
