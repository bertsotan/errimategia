#!/usr/bin/env python3

###############################################################################
#
# Script honek esaldi garbietan letra larriren bat duten hitz berriak antzeman
# eta eskuz berrikusi ahal izateko gordetzen ditu. Gidoidunak eta gidoi
# gabeko hitzak hautatzen ditu letra larrien bat baldin badute.
#
# Besteak beste ondokoak egiten ditu:
#   - "izenak" fitxategiak sintaxia betetzen duela ziurtatu.
#   - "esaldiak-garbi" fitxategitik letra larrian dituzten hitzak lortu.
#   - "izenak" fitxategia aldatu aurretik bere babeskopia egin.
#   - Aurreko informazioarekin "izenak" fitxategia berregin.
#
# Sarrerak eta irteerak:
#    <-- input/izenak-header
#    <-- input-output/esaldiak-garbi
#    <-> input-output/izenak
#    --> input-output/izenak.back
#    --> (pantaila)
#
###############################################################################

import datetime
import shutil
import os
from lib import (
    lortu_proiektuaren_bidea
)

# Fitxategiak
proiektuarenBidea = lortu_proiektuaren_bidea()
inputEsaldiakBidea = proiektuarenBidea + "/input-output/esaldiak-garbi"
inputIzenakHeaderBidea = proiektuarenBidea + "/input/izenak-header"
inputOutputIzenakBidea = proiektuarenBidea + "/input-output/izenak"
outputIzenakBabeskopiaBidea = inputOutputIzenakBidea + ".back"

hasiDataOrdua = datetime.datetime.now()
print("HASI - " + str(hasiDataOrdua))

###############################################################################

egungoIzenakZerrenda = list()

print("\nEgungo letra larridun hitzak lortzen (izenak)...")

inputIzenakFitxategia = open(inputOutputIzenakBidea, 'r')

while True:
    lerroa = inputIzenakFitxategia.readline()

    egungoIzena = dict()

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    if lerroa != '' and not lerroa.isspace() and not lerroa.startswith('#') and not lerroa.endswith('-'):
        lerrokoEragiketa = ''
        lerrokoHitza = lerroa
        if len(lerroa) >= 2 and (lerroa[0] == '+' or lerroa[0] == '-'):
            if lerroa[1] != ' ':
                continue
            lerrokoEragiketa = lerroa[0]
            if len(lerroa) <= 2:
                lerrokoHitza = ''
            else:
                lerrokoHitza = lerroa[2:len(lerroa)]
        egungoIzena["eragiketa"] = lerrokoEragiketa
        egungoIzena["hitza"] = lerrokoHitza

        egungoIzenakZerrenda.append(egungoIzena)

print(
    "\tAurkitutako izenak, hau da, letra larriren bat duten hitzak: " +
    str(len(egungoIzenakZerrenda))
)

inputIzenakFitxategia.close()

###############################################################################

esaldietakoIzenakZerrenda = list()

print("\nDeskargatutako esaldien izenak lortzen (esaldiak-garbi)...")

inputEsaldiakFitxategia = open(inputEsaldiakBidea, 'r')

while True:
    lerroa = inputEsaldiakFitxategia.readline()

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    if lerroa.isspace():
        break

    if lerroa.startswith("#"):
        break

    lerrokoHitzak = lerroa.split(' ')
    for hitza in lerrokoHitzak:
        if not hitza.islower():
            if len(hitza) >= 2 and hitza not in esaldietakoIzenakZerrenda:
                esaldietakoIzenakZerrenda.append(hitza)

print(
    "\tDeskargatutako izenak: " +
    str(len(esaldietakoIzenakZerrenda))
)

inputEsaldiakFitxategia.close()

###############################################################################

print("\nIzenen zerrenda bateratu berria prestatzen...")

# egungoIzenakZerrenda konplexuatik (list(dict())) zerrenda sinplea lortu
egungoIzenakZerrendaSinplea = list()
for egungoIzena in egungoIzenakZerrenda:
    egungoIzenakZerrendaSinplea.append(egungoIzena["hitza"])

izenBerriakZerrenda = list()
berriKopurua = 0
for esaldietakoIzena in esaldietakoIzenakZerrenda:
    if esaldietakoIzena not in egungoIzenakZerrendaSinplea:
        izenBerria = dict()
        izenBerria["eragiketa"] = ''
        izenBerria["hitza"] = esaldietakoIzena

        izenBerriakZerrenda.append(izenBerria)
        berriKopurua += 1

[egungoIzenakZerrenda.append(izena) for izena in izenBerriakZerrenda]

print(
    "\tGehitutako izen berriak: " +
    str(berriKopurua)
)

###############################################################################

print("\nIzenen zerrenda alfabetikoki ordenatzen...")

egungoIzenakZerrenda = sorted(
    egungoIzenakZerrenda,
    key=lambda izena: izena["hitza"].casefold()
)

###############################################################################

print(
    "\nFitxategi baten babeskopia egiten...\n" +
    "\tJatorria: " + inputOutputIzenakBidea + "\n" +
    "\tKopia:    " + outputIzenakBabeskopiaBidea)

shutil.copy(
    inputOutputIzenakBidea,
    outputIzenakBabeskopiaBidea)

###############################################################################

print("\nIzenen zerrenda berria gordetzen (izenak)...")

outputIzenakFitxategia = open(inputOutputIzenakBidea, 'w')

print("\n\tGoiburua gordetzen...")

inputIzenakHeaderFitxategia = open(inputIzenakHeaderBidea, 'r')

while True:
    lerroa = inputIzenakHeaderFitxategia.readline()

    if not lerroa:
        break

    outputIzenakFitxategia.write(lerroa)

inputIzenakHeaderFitxategia.close()

print("\nIzenak gordetzen...")

for egungoIzena in egungoIzenakZerrenda:
    if egungoIzena["eragiketa"] == '':
        lerroa = egungoIzena["hitza"]
    else:
        lerroa = egungoIzena["eragiketa"] + ' ' + egungoIzena["hitza"]
    outputIzenakFitxategia.write("\n" + lerroa)
print("\tIzenen kopuru eguneratua: " + str(len(egungoIzenakZerrenda)) + " (" + str(len(egungoIzenakZerrenda)-len(egungoIzenakZerrenda)) + " gehiago)")

outputIzenakFitxategia.close()

###############################################################################

amaituDataOrdua = datetime.datetime.now()
print(
    "\nAMAITU - " + str(amaituDataOrdua) +
    " (" + str(amaituDataOrdua - hasiDataOrdua) + ")"
)
