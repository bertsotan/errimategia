#!/usr/bin/env python3

import re
import sys
import os
from inspect import getsourcefile
from typing import Tuple


def lortu_proiektuaren_bidea():
    bidea = os.path.dirname(getsourcefile(lambda: 0))  # type: ignore

    if str(bidea).endswith("/."):
        bidea = bidea[0:len(bidea)-2]
    bidea = os.path.dirname(os.path.dirname(bidea))  # /bin/lib kentzeko

    return bidea


def lortu_ahoskera1(ahoskera: str) -> str:
    # Ahoskera zorrotza (hizkiz hizki)

    # 1) Bazterretako hutsuneak kendu
    ahoskera1 = ahoskera.strip()

    # 2) anycase -> lowercase
    ahoskera1 = ahoskera1.lower()

    # 3) ll hasieran -> i
    # ^ll,  ll -> i Llorente
    if ahoskera1.startswith("ll"):  # Testuaren hasieran
        ahoskera1 = "i" + ahoskera1[len("ll"):len(ahoskera1)]
    ahoskera1 = ahoskera1.replace(' ll', 'i')  # Hitzaren hasieran

    # 4) Tarteko hutsuneak kendu
    ahoskera1 = ahoskera1.replace(' ', '')

    # 5) Gainontzeko ordezkapenak
    # ll -> l Apollo
    ahoskera1 = ahoskera1.replace('ll', 'l')
    # á, é, í, ó, ú -> a, e, i, o, u
    ahoskera1 = ahoskera1.replace('á', 'a')
    ahoskera1 = ahoskera1.replace('é', 'e')
    ahoskera1 = ahoskera1.replace('í', 'i')
    ahoskera1 = ahoskera1.replace('ó', 'o')
    ahoskera1 = ahoskera1.replace('ú', 'u')
    # bb -> b
    ahoskera1 = ahoskera1.replace('bb', 'b')
    # tt -> t
    ahoskera1 = ahoskera1.replace('tt', 't')
    # dd -> d
    ahoskera1 = ahoskera1.replace('dd', 'd')
    # ss -> s
    ahoskera1 = ahoskera1.replace('ss', 's')
    # gg -> g
    ahoskera1 = ahoskera1.replace('gg', 'g')
    # mm -> m
    ahoskera1 = ahoskera1.replace('mm', 'm')
    # nn -> n
    ahoskera1 = ahoskera1.replace('nn', 'n')
    # pp -> p
    ahoskera1 = ahoskera1.replace('pp', 'p')
    # zz -> tz
    ahoskera1 = ahoskera1.replace('zz', 'tz')
    # ñ -> in Ikurriña, Egaña
    ahoskera1 = ahoskera1.replace('ñ', 'in')  # TODO Ega\u00f1a
    # ck -> k
    ahoskera1 = ahoskera1.replace('ck', 'k')
    # ph -> f
    ahoskera1 = ahoskera1.replace('ph', 'f')
    # qui -> ki
    ahoskera1 = ahoskera1.replace('qui', 'ki')
    # que -> ke
    ahoskera1 = ahoskera1.replace('que', 'ke')
    # q -> k
    ahoskera1 = ahoskera1.replace('q', 'k')
    # tch -> tx Tatcher
    ahoskera1 = ahoskera1.replace('tch', 'tx')
    # ch -> tx Pinochet
    ahoskera1 = ahoskera1.replace('ch', 'tx')
    # c -> k
    ahoskera1 = ahoskera1.replace('c', 'k')
    # v -> b
    ahoskera1 = ahoskera1.replace('v', 'b')
    # w -> u Twit, Willow, Wifi
    ahoskera1 = ahoskera1.replace('w', 'u')
    # ii -> i
    ahoskera1 = ahoskera1.replace('ii', 'i')
    # y -> i Yeste, Trinity
    ahoskera1 = ahoskera1.replace('y', 'i')
    # ' ->
    ahoskera1 = ahoskera1.replace('\'', '')
    # h ->
    ahoskera1 = ahoskera1.replace('h', '')
    # Bokal berdina bikoiztuta badago, bakarra utzi.
    # Adibideak: mahai, ama-alaba, Santa Ageda
    ahoskera1 = ahoskera1.replace('aa', 'a')
    ahoskera1 = ahoskera1.replace('ee', 'e')  # green!
    ahoskera1 = ahoskera1.replace('ii', 'i')
    ahoskera1 = ahoskera1.replace('oo', 'o')  # moon!
    ahoskera1 = ahoskera1.replace('uu', 'u')

    # th -> θ ????????????? Elisabeth
    # gui -> gi Ezin da automatikoki ordezkatu! (pinguino)
    # gue -> ge Ezin da automatikoki ordezkatu! (eguerdi)
    # ing -> in Ezin da automatikoki ordezkatu! (ingelesa)
    # ee -> i Ezin da automatikoki ordezkatu! (zitekeen)
    # oo -> u Ezin da automatikoki ordezkatu! (kooperazioa, zoo, urkook)
    # TODO

    # 6) Gidoiak kendu
    # - ->
    ahoskera1 = ahoskera1.replace('-', '')

    return ahoskera1


def lortu_ahoskera2(ahoskera: str) -> str:
    # Ahoskera normala (errima familiaka)

    # 1) Ahoskera1 lortu
    ahoskera2 = lortu_ahoskera1(ahoskera)

    # 2) begirada: b, g, d, r -> B
    ahoskera2 = ahoskera2.replace('b', 'B')
    ahoskera2 = ahoskera2.replace('g', 'B')
    ahoskera2 = ahoskera2.replace('d', 'B')

    # 3) r: Aurretik edo ondoren bokala ez den zerbait badator ez ordezkatu
    if ahoskera2.startswith("r"):  # Testuaren hasieran
        ahoskera2 = "B" + ahoskera2[len("r"):len(ahoskera2)]
    ahoskera2 = ahoskera2.replace(' r', 'B')  # Hitzaren hasieran
    ahoskera2 = re.sub("([^r])r([^r])", "\\1B\\2", ahoskera2)  # Tartean
    if ahoskera2.endswith("r"):
        ahoskera2 = ahoskera2[:-len("r")] + "B"  # Testuaren amaieran
    # TODO gabriel -> gaBBiel!

    # 4) kopeta: k, p, t -> T
    ahoskera2 = ahoskera2.replace('k', 'K')
    ahoskera2 = ahoskera2.replace('p', 'K')
    # t: Ondoren z, s edo x badator ez ordezkatu
    ahoskera2 = re.sub("t([^zsx])", "K\\1", ahoskera2)  # Tartean
    if ahoskera2.endswith("t"):
        ahoskera2 = ahoskera2[:-len("t")] + "K"  # Testuaren amaieran

    # 5) amona: m, n -> M
    ahoskera2 = ahoskera2.replace('m', 'M')
    ahoskera2 = ahoskera2.replace('n', 'M')

    # 6) sugea: z, s, x, tz, ts, tx -> Z
    ahoskera2 = ahoskera2.replace('tz', 'Z')
    ahoskera2 = ahoskera2.replace('ts', 'Z')
    ahoskera2 = ahoskera2.replace('tx', 'Z')
    ahoskera2 = ahoskera2.replace('z', 'Z')
    ahoskera2 = ahoskera2.replace('s', 'Z')
    ahoskera2 = ahoskera2.replace('x', 'Z')

    # TODO kontsonante ahulak

    return ahoskera2


def lortu_azken3ak(hitza: str) -> str:
    if len(hitza) == 2:
        azken3ak = hitza[
            len(hitza)-2:
            len(hitza)]
    else:
        azken3ak = hitza[
            len(hitza)-3:
            len(hitza)]

    return azken3ak


def lortu_bukaera1(bukaera: str) -> str:
    ahoskera1 = lortu_ahoskera1(bukaera)

    bukaera1 = lortu_azken3ak(ahoskera1)

    return bukaera1


def lortu_bukaera2(bukaera: str) -> str:
    ahoskera2 = lortu_ahoskera2(bukaera)

    if len(ahoskera2) < 2:
        sys.exit("ERROREA (lortu_bukaera2): bukaera laburregia da.")

    bukaera2 = lortu_azken3ak(ahoskera2)

    return bukaera2


def lortu_hitza_lerrotik(lerroa: str) -> Tuple[dict, dict]:

    if not lerroa:
        sys.exit("ERROREA (lortu_hitza): lerrorik ez.")

    if lerroa.isspace():
        sys.exit("ERROREA (lortu_hitza): lerroan hutsuneak.")

    if len(lerroa) == 0:
        sys.exit("ERROREA (lortu_hitza): lerroa hutsik.")

    lerroko_hitza = ""
    ahoskera = ""
    ahoskera_eskuzkoa_da = False
    erabilgarritasuna = -1
    polisemikoa_da = False
    lokala_da = False
    lagunartekoa_da = False
    erderakada_da = False
    deklinatuta_dago = False
    gaiak = list()
    iruzkina = ""

    lerroko_gaiak = ""
    lerroko_markak = ""

    polisemiko_kopurua = 0
    lokal_kopurua = 0
    lagunarteko_kopurua = 0
    erderakada_kopurua = 0
    deklinatu_kopurua = 0

    # <hitza(k)> <ahoskera> <erabilgarritasuna> <marka(k)> <gaia(k)> <iruzkina>

    if lerroa.find('#') > 0:
        # Iruzkina
        iruzkina = lerroa[lerroa.find('#') + 1: len(lerroa)].strip()
        lerroa = lerroa[0:lerroa.find('#')].strip()

    if lerroa.find('{') > 0:
        # Gaiak
        lerroko_gaiak = lerroa[lerroa.find('{'): len(lerroa)].strip()
        # print("\tGaiak: " + lerroaGaiak)
        lerroa = lerroa[0:lerroa.find('{')].strip()

        # Gaiak banatu eta garbitu
        lerroko_gaiak = lerroko_gaiak.replace('}', '} ')
        gaiak = lerroko_gaiak.split(' ')
        gaiak = [x for x in gaiak if x != '']  # Gai hutsak kendu
        i = 0
        for i in range(len(gaiak)):
            gaiak[i] = gaiak[i].replace('{', '').replace('}', '')
        gaiak.sort()

    if lerroa.find('(') > 0:
        # Markak
        lerroko_markak = lerroa[lerroa.find('(') + 1: len(lerroa) - 1].strip()
        lerroa = lerroa[0:lerroa.find('(')].strip()
        # Markak banatu eta garbitu
        lerroko_markak = lerroko_markak.replace('(', '')
        lerroko_markak = lerroko_markak.replace(')', '')
        markak = lerroko_markak.split(' ')
        markak = [x for x in markak if x != '']  # Marka hutsak kendu
        i = 0
        for i in range(len(markak)):
            if not markak[i]:
                continue
            # print("\t\tmarkak[i]: " + markak[i])
            match markak[i]:
                case "pol":
                    polisemikoa_da = True
                case "lok":
                    lokala_da = True
                case "lag":
                    lagunartekoa_da = True
                case "erd":
                    erderakada_da = True
                case "dek":
                    deklinatuta_dago = True
                case _:
                    sys.exit("ERROREA (lortu_hitza): marka ezezaguna: " +
                             markak[i])

    if lerroa.rfind(' ') > 0 and re.search(
            "\d", lerroa[lerroa.rfind(' ') + 1]):
        # Erabilgarritasuna
        erabilgarritasuna = int(lerroa[
            lerroa.rfind(' ') + 1:
            len(lerroa)
        ].strip())
        # print("\tErabilgarritasuna: " + str(erabilgarritasuna))
        lerroa = lerroa[0:lerroa.rfind(' ')].strip()

    if lerroa.find('[') > 0:
        # Ahoskera
        ahoskera = lerroa[lerroa.find('[') + 1: len(lerroa) - 1].strip()
        # print("\tAhoskera: " + ahoskera)
        ahoskera_eskuzkoa_da = True
        lerroa = lerroa[0:lerroa.find('[')].strip()
    # Hitza
    lerroko_hitza = lerroa
    # print("\tHitza: " + lerroa)

    # Ahoskera
    if not ahoskera:
        ahoskera = lerroko_hitza
    ahoskera1 = lortu_ahoskera1(ahoskera)
    ahoskera2 = lortu_ahoskera2(ahoskera)

    # Hitza objektua

    hitza = dict()

    hitza["hitza"] = lerroko_hitza
    hitza["maiuskularikDu"] = lerroko_hitza != lerroko_hitza.lower()
    if lerroko_hitza.find(' ') > 0:
        hitza["hutsunerikDu"] = True
    else:
        hitza["hutsunerikDu"] = False
    hitza["ahoskera1"] = ahoskera1
    hitza["ahoskera2"] = ahoskera2
    hitza["ahoskeraEskuzkoaDa"] = ahoskera_eskuzkoa_da
    hitza["bukaera"] = lortu_bukaera1(ahoskera1)
    if erabilgarritasuna == -1:
        hitza["erabilgarritasuna"] = 2
    else:
        hitza["erabilgarritasuna"] = erabilgarritasuna
    if polisemikoa_da:
        hitza["polisemikoaDa"] = polisemikoa_da
        polisemiko_kopurua += 1
    if lokala_da:
        hitza["lokalaDa"] = lokala_da
        lokal_kopurua += 1
    if lagunartekoa_da:
        hitza["lagunartekoaDa"] = lagunartekoa_da
        lagunarteko_kopurua += 1
    if erderakada_da:
        hitza["erderakadaDa"] = erderakada_da
        erderakada_kopurua += 1
    if deklinatuta_dago:
        hitza["deklinatutaDago"] = deklinatuta_dago
        deklinatu_kopurua += 1
    if len(gaiak) > 0:
        hitza["gaiak"] = gaiak
    if iruzkina:
        hitza["iruzkina"] = iruzkina

    estatistikak = dict()
    estatistikak["polisemikoak"] = polisemiko_kopurua
    estatistikak["lokalak"] = lokal_kopurua
    estatistikak["lagunartekoak"] = lagunarteko_kopurua
    estatistikak["erderakadak"] = erderakada_kopurua
    estatistikak["deklinatuak"] = deklinatu_kopurua

    return hitza, estatistikak


def lortu_lerroa_hitzetik(hitza: dict) -> str:
    lerroa = ""

    lerroa = hitza["hitza"]

    if "ahoskeraEskuzkoaDa" in hitza:
        if hitza["ahoskeraEskuzkoaDa"]:
            lerroa += " [" + hitza["ahoskera1"] + "]"

    if hitza["erabilgarritasuna"] != 2:
        lerroa += " " + str(hitza["erabilgarritasuna"])

    if "polisemikoaDa" in hitza:
        lerroa += " (pol)"

    if "lokalaDa" in hitza:
        lerroa += " (lok)"

    if "lagunartekoaDa" in hitza:
        lerroa += " (lag)"

    if "erderakadaDa" in hitza:
        lerroa += " (erd)"

    if "deklinatutaDago" in hitza:
        lerroa += " (dek)"

    if "gaiak" in hitza:
        for gaia in hitza["gaiak"]:
            lerroa += " {" + gaia + "}"

    if "iruzkina" in hitza:
        lerroa += " #" + hitza["iruzkina"]

    return lerroa


def bilatu_hitza_zerrendan(hitza_bera, hitzenZerrenda):
    return [hitza for hitza in hitzenZerrenda if hitza['hitza'] == hitza_bera]
