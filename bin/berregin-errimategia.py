#!/usr/bin/env python3

###############################################################################
#
# Script honek "errimategia" fitxategia berregiten du, atal guztiak irakurriz
# eta automatikoki berridatziz: goiburua, gaien zerrenda, bukaerak eta hitzak.
#
# Besteak beste ondokoak egiten ditu:
#   - "izenak" fitxategiak sintaxia betetzen duela ziurtatu.
#   - "gidoidunak" fitxategiak sintaxia betetzen duela ziurtatu.
#   - "errimategia" fitxategia aldatu aurretik bere babeskopia egin.
#   - Aurreko informazioarekin "errimategia" fitxategia berregin, goiburua
#     txertatuz, gaiak zerrendatuz eta azkenik bukaerak eta berauen errimak
#     zerrendatuz.
#
# Sarrerak eta irteerak:
#     <-- input-output/izenak
#     <-- input-output/gidoidunak
#     <-- input/errimategia-header
#     <-> input-output/errimategia
#     --> (pantaila)
#
###############################################################################

import datetime
import re
import sys
import shutil
import os
from lib import (
    lortu_proiektuaren_bidea,
    lortu_ahoskera1,
    lortu_ahoskera2,
    lortu_azken3ak,
    lortu_bukaera1,
    lortu_bukaera2,
    lortu_hitza_lerrotik,
    lortu_lerroa_hitzetik,
    bilatu_hitza_zerrendan
)

# Fitxategiak
proiektuarenBidea = lortu_proiektuaren_bidea()
inputErrimategiaHeaderBidea = proiektuarenBidea + "/input/errimategia-header"
inputOutputErrimategiaBidea = proiektuarenBidea + "/input-output/errimategia"
inputOutputErrimategiaBabeskopiaBidea = inputOutputErrimategiaBidea + ".back"

hasiDataOrdua = datetime.datetime.now()
print("HASI - " + str(hasiDataOrdua))

###############################################################################

print(
    "\nErrimategiaren sintaxia egiaztatzen...\t" +
    inputOutputErrimategiaBidea
)

inputOutputErrimategia = open(
    inputOutputErrimategiaBidea,
    'r'
)

errimategikoHitzak = list()

lerroZenbakia = 0
abisuKopurua = 0
while True:
    lerroa = inputOutputErrimategia.readline()
    lerroZenbakia = lerroZenbakia + 1

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    # Sintaxia:
    #   # Iruzkina
    #   <hitza(k)> <ahoskera> <maiztasuna> <marka(k)> <gaia(k)> # Iruzkina
    patroia = "(^$|^(#.*|" + "[\s\da-zA-Z\-ñÑáéíóúÁÉÍÓÚ\.']*" + "\s*"  # Hitzak
    patroia += "(\[[\sa-zA-Z\-ñÑáéíóúÁÉÍÓÚθ]*\])?" + "\s*"  # Ahoskera
    patroia += "\d*" + "\s*"  # Maiztasuna
    patroia += "(\((pol|lok|lag|erd|dek)\)\s*)*" + "\s*"  # Markak
    patroia += "(\{[\s\da-zA-Z\-ñÑáéíóúÁÉÍÓÚ]*\}\s*)*" + "\s*"  # Gaiak
    patroia += "(#[^\"]*)?" + ")$)"  # Iruzkina
    if not re.search(patroia, lerroa):
        sys.exit("\t" + str(lerroZenbakia) +
                 ". lerroak ez du sintaxia betetzen: " + lerroa)
    # Beraz, lerroak sintaxia betetzen du

    if lerroa and lerroa[0] != "#":
        if lerroa.find("[") != -1:
            ahoskera1 = lerroa[lerroa.find("["):lerroa.find("]")]
            if len(ahoskera1) <= 2:
                sys.exit("\tERROREA: " + str(lerroZenbakia) +
                         ". lerroko hitzaren ahoskera laburregia da: " +
                         lerroa)
            # Beraz, ahoskerak gutxieneko luzera betetzen du

            hitza, estatistikak = lortu_hitza_lerrotik(lerroa)
        else:
            hitza, estatistikak = lortu_hitza_lerrotik(lerroa)
            if len(hitza) <= 2:
                sys.exit("\tERROREA: " + str(lerroZenbakia) +
                         ". lerroko hitza laburregia da: " + lerroa)
            # Beraz, hitzak gutxieneko luzera betetzen du

        # if hitza in errimategikoHitzak:
        if len(bilatu_hitza_zerrendan(
            hitza["hitza"], errimategikoHitzak
        )) != 0:
            abisuKopurua += 1
            print("\tABISUA: " + str(lerroZenbakia) +
                  ". lerroko hitza errepikatuta dago (goragoko " +
                  "zehazten da): " + lerroa)
        else:
            errimategikoHitzak.append(hitza)

if abisuKopurua > 0:
    sys.exit("\tGoiko arazoak konpondu arte ezin da errimategia berregin")
else:
    print("\tDena ondo")

inputOutputErrimategia.close()

###############################################################################

print(
    "\nFitxategi baten babeskopia egiten...\n" +
    "\tJatorria: " + inputOutputErrimategiaBidea + "\n" +
    "\tKopia:    " + inputOutputErrimategiaBabeskopiaBidea
)

egungoBideOsoa = os.getcwd()

shutil.copy(
    inputOutputErrimategiaBidea,
    inputOutputErrimategiaBabeskopiaBidea
)

###############################################################################

print("\nErrimategia irakurtzen:")

print("\n\tAurkitutako gai desberdinak zerrendatzen...")

errimategikoGaiak = list()

inputErrimategiaBabeskopiaFitxategia = open(
    inputOutputErrimategiaBabeskopiaBidea,
    'r'
)

inputErrimategiaBabeskopiaFitxategia.seek(0)

while True:
    lerroa = inputErrimategiaBabeskopiaFitxategia.readline()

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    if lerroa and lerroa[0] != "#":
        lerrokoGaiak = re.findall(r'\{([^}]*)\}', lerroa)

        for lerrokoGaia in lerrokoGaiak:
            if lerrokoGaia not in errimategikoGaiak:
                errimategikoGaiak.append(lerrokoGaia)

errimategikoGaiak.sort()

for errimategikoGaia in errimategikoGaiak:
    print("\t\t - " + errimategikoGaia)

###############################################################################

print("\n\tErrimategiko errimak lortzen...")

errimategikoHitzak = list()

inputErrimategiaBabeskopiaFitxategia.seek(0)

while True:
    lerroa = inputErrimategiaBabeskopiaFitxategia.readline()

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    if len(lerroa) == 0:
        continue

    # Lerro osoa iruzkina bada, saltatu lerroa
    if lerroa[0] == "#":
        continue

    # print ("\tLerroa: " + lerroa)

    hitza, statistikak = lortu_hitza_lerrotik(lerroa)

    if len(hitza["ahoskera1"]) > 2:  # "hi" bezalako hitzak ezikusi
        errimategikoHitzak.append(hitza)

print("\t\tAurkitutako errimak: " + str(len(errimategikoHitzak)))

inputErrimategiaBabeskopiaFitxategia.close()

###############################################################################

print("\n\tErrimategiko bukaeren zerrenda lortzen...")

errimategikoBukaerak = list()

for hitza in errimategikoHitzak:
    bukaeraGehitu = True
    for errimaBukaera in errimategikoBukaerak:
        if errimaBukaera["ahoskera1"] == hitza["bukaera"]:
            bukaeraGehitu = False
            break
    if bukaeraGehitu:
        ahoskera2 = lortu_bukaera2(hitza["bukaera"])
        errimaBukaeraBerria = dict()
        errimaBukaeraBerria["ahoskera1"] = hitza["bukaera"]
        errimaBukaeraBerria["ahoskera2"] = ahoskera2
        # print(errimaBukaeraBerria)
        errimategikoBukaerak.append(errimaBukaeraBerria)

errimategikoBukaerak = sorted(
    errimategikoBukaerak,
    key=lambda errimaBukaera: errimaBukaera["ahoskera1"]
)

print("\t\tAurkitutako bukaerak: " + str(len(errimategikoBukaerak)))

###############################################################################

print("\nErrimategia gordetzen...\t" + inputOutputErrimategiaBidea)

outputErrimategia = open(inputOutputErrimategiaBidea, 'w')

print("\n\tGoiburua gordetzen...")

inputErrimategiaHeader = open(inputErrimategiaHeaderBidea, 'r')

while True:
    lerroa = inputErrimategiaHeader.readline()

    if not lerroa:
        break

    outputErrimategia.write(lerroa)

inputErrimategiaHeader.close()

outputErrimategia.write("\n")

print("\n\tGaiak gordetzen...")

outputErrimategia.write("\n# Gaiak:\n")

for errimategikoGaia in errimategikoGaiak:
    outputErrimategia.write("#   {" + errimategikoGaia + "}\n")

print("\n\tHitzak gorde aurretik ordenatzen...")

# Ondo legoke gako boolear batzuen arabera ere ordenatzea, baina ezin da
keys = ['bukaera', 'erabilgarritasuna', 'hitza']
orders = [1, 1, 1, 1]
gordetakoHitzOrdenatuak = sorted(
    errimategikoHitzak,
    key=lambda x: [order * x[key] for (key, order) in zip(keys, orders)]
)

if True:
    print("\n\tHitzak alfabetikoki gordetzen...")

    gordetakoHitzOrdenatuak = sorted(
        gordetakoHitzOrdenatuak,
        key=lambda izena: izena["hitza"].casefold()
    )

    for hitza in gordetakoHitzOrdenatuak:
        lerroa = lortu_lerroa_hitzetik(hitza)
        outputErrimategia.write(lerroa + "\n")

else:
    print("\n\tHitzak bukaeraka gordetzen...")

    for bukaera in errimategikoBukaerak:
        outputErrimategia.write(
            "\n# Bukaera: -" + bukaera["ahoskera1"] + "\n"
        )
        outputErrimategia.write("########################################\n\n")

        # - Polisemikoak direnak
        #   - A) Maiuskularik ez dutenak
        #   - B) Maiuskulak badituztenak
        # - Polisemikoak ez direnak
        #   - Maiuskularik ez dutenak
        #     - 1 edo 2 erabilgarritasuna dutenak
        #       - C) Hutsuneak dituztenak
        #       - D) Hutsunerik ez duten Deklinatu gabeak
        #     - 3 erabilgarritasuna dutenak
        #       - E) Hutsuneak dituztenak
        #       - F) Hutsunerik ez duten Deklinatu gabeak
        #     - G) Hutsunerik ez duten Deklinatuak
        #   - Maiuskulak badituztenak
        #     - H) Deklinatu gabeak
        #     - I) Deklinatuak

        for hitza in gordetakoHitzOrdenatuak:
            if hitza["bukaera"] == bukaera["ahoskera1"]:
                # A) Polisemikoa bai + Maiuskulak ez
                if ("polisemikoaDa" in hitza and
                hitza["hitza"] == hitza["hitza"].lower()):
                    lerroa = lortu_lerroa_hitzetik(hitza)
                    outputErrimategia.write(lerroa + "\n")

        for hitza in gordetakoHitzOrdenatuak:
            if hitza["bukaera"] == bukaera["ahoskera1"]:
                # B) Polisemikoa bai + Maiuskulak bai
                if ("polisemikoaDa" in hitza and
                hitza["hitza"] != hitza["hitza"].lower()):
                    lerroa = lortu_lerroa_hitzetik(hitza)
                    outputErrimategia.write(lerroa + "\n")

        for hitza in gordetakoHitzOrdenatuak:
            if hitza["bukaera"] == bukaera["ahoskera1"]:
                # C) Polisemikoa ez + Maiuskulak ez + Erabilgarritasuna 1/2 +
                #    Hutsuneak bai
                if ("polisemikoaDa" not in hitza and
                hitza["hitza"] == hitza["hitza"].lower() and
                1 <= hitza["erabilgarritasuna"] <= 2 and
                hitza["hitza"].find(" ") > 0):
                    lerroa = lortu_lerroa_hitzetik(hitza)
                    outputErrimategia.write(lerroa + "\n")

        for hitza in gordetakoHitzOrdenatuak:
            if hitza["bukaera"] == bukaera["ahoskera1"]:
                # D) Polisemikoa ez + Maiuskulak ez + Erabilgarritasuna 1/2 +
                #    Hutsuneak ez + Deklinatua ez
                if ("polisemikoaDa" not in hitza and
                hitza["hitza"] == hitza["hitza"].lower() and
                1 <= hitza["erabilgarritasuna"] <= 2 and
                hitza["hitza"].find(" ") == -1 and
                "deklinatutaDago" not in hitza):
                    lerroa = lortu_lerroa_hitzetik(hitza)
                    outputErrimategia.write(lerroa + "\n")

        for hitza in gordetakoHitzOrdenatuak:
            if hitza["bukaera"] == bukaera["ahoskera1"]:
                # E) Polisemikoa ez + Maiuskulak ez + Erabilgarritasuna 3 +
                #    Hutsuneak bai
                if ("polisemikoaDa" not in hitza and
                hitza["hitza"] == hitza["hitza"].lower() and
                hitza["erabilgarritasuna"] == 3 and
                hitza["hitza"].find(" ") > 0):
                    lerroa = lortu_lerroa_hitzetik(hitza)
                    outputErrimategia.write(lerroa + "\n")

        for hitza in gordetakoHitzOrdenatuak:
            if hitza["bukaera"] == bukaera["ahoskera1"]:
                # F) Polisemikoa ez + Maiuskulak ez + Erabilgarritasuna 3 +
                #    Hutsuneak ez + Deklinatua ez
                if ("polisemikoaDa" not in hitza and
                hitza["hitza"] == hitza["hitza"].lower() and
                hitza["erabilgarritasuna"] == 3 and
                hitza["hitza"].find(" ") == -1 and
                "deklinatutaDago" not in hitza):
                    lerroa = lortu_lerroa_hitzetik(hitza)
                    outputErrimategia.write(lerroa + "\n")

        for hitza in gordetakoHitzOrdenatuak:
            if hitza["bukaera"] == bukaera["ahoskera1"]:
                # G) Polisemikoa ez + Maiuskulak ez + Hutsuneak ez + Deklinatua bai
                if ("polisemikoaDa" not in hitza and
                hitza["hitza"] == hitza["hitza"].lower() and
                hitza["hitza"].find(" ") == -1 and
                "deklinatutaDago" in hitza):
                    lerroa = lortu_lerroa_hitzetik(hitza)
                    outputErrimategia.write(lerroa + "\n")

        for hitza in gordetakoHitzOrdenatuak:
            if hitza["bukaera"] == bukaera["ahoskera1"]:
                # H) Polisemikoa ez + Maiuskulak bai + Deklinatua ez
                if ("polisemikoaDa" not in hitza and
                hitza["hitza"] != hitza["hitza"].lower() and
                "deklinatutaDago" not in hitza):
                    lerroa = lortu_lerroa_hitzetik(hitza)
                    outputErrimategia.write(lerroa + "\n")

        for hitza in gordetakoHitzOrdenatuak:
            if hitza["bukaera"] == bukaera["ahoskera1"]:
                # I) Polisemikoa ez + Maiuskulak bai + Deklinatua bai
                if ("polisemikoaDa" not in hitza and
                hitza["hitza"] != hitza["hitza"].lower() and
                "deklinatutaDago" in hitza):
                    lerroa = lortu_lerroa_hitzetik(hitza)
                    outputErrimategia.write(lerroa + "\n")

outputErrimategia.close()

###############################################################################

amaituDataOrdua = datetime.datetime.now()
print(
    "\nAMAITU - " + str(amaituDataOrdua) +
    " (" + str(amaituDataOrdua - hasiDataOrdua) + ")"
)
