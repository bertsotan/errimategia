#!/usr/bin/env python3

###############################################################################
#
# Script honek 3 fitxategitako hitzak egiaztatu eta errimategiaren bukaeran
# gehitzen ditu.
#
# Besteak beste ondokoak egiten ditu:
#   - "gidoidunak" fitxategiak sintaxia betetzen duela ziurtatu.
#   - "izenak" fitxategiak sintaxia betetzen duela ziurtatu.
#   - "errimak" fitxategiak sintaxia betetzen duela ziurtatu.
#   - Fitxategian aurkitutako hitzak "errimategia" fitxategiaren bukaeran
#     gehitu, dagoeneko errimategian daudenak ezikusiz.
#
# Sarrerak eta irteerak:
#    <-- input-output/gidoidunak
#    <-- input-output/izenak
#    <-- input-output/errimak
#    <-> input-output/errimategia
#    --> (pantaila)
#
###############################################################################

import datetime
import re
import sys
import shutil
import os
from lib import (
    lortu_proiektuaren_bidea,
    lortu_ahoskera1,
    lortu_ahoskera2,
    lortu_azken3ak,
    lortu_bukaera1,
    lortu_bukaera2,
    lortu_hitza_lerrotik,
    lortu_lerroa_hitzetik,
    bilatu_hitza_zerrendan
)

proiektuarenBidea = lortu_proiektuaren_bidea()
inputGidoidunakBidea = proiektuarenBidea + "/input-output/gidoidunak"
inputIzenakBidea = proiektuarenBidea + "/input-output/izenak"
inputErrimakBidea = proiektuarenBidea + "/input-output/errimak"
inputOutputErrimategiaBidea = proiektuarenBidea + "/input-output/errimategia"
inputOutputErrimategiaBabeskopiaBidea = inputOutputErrimategiaBidea + ".back"

hasiDataOrdua = datetime.datetime.now()
print("HASI - " + str(hasiDataOrdua))

###############################################################################

print("\nSintaxia egiaztatzen...\t " + inputGidoidunakBidea)

inputGidoidunakFitxategia = open(inputGidoidunakBidea, 'r')

gidoidunHitzak = list()

lerroZenbakia = 0
while True:
    lerroa = inputGidoidunakFitxategia.readline()
    lerroZenbakia = lerroZenbakia + 1

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '').strip()

    # Onartzen dira: lerro hutsa, iruzkina, hitza.
    # Lerroko lehen karakterea ezin da hutsunea izan
    # Hitzak gidoiren bat izan behar du
    if lerroa and lerroa[0] != "#":
        if lerroa.find("#") >= 0:
            lerroa = lerroa[
                0:
                lerroa.find("#") - 1
            ]
            lerroa = lerroa.strip()

        if not re.search(
            "(^$|^[\+|\-] (#.*|[a-zA-Z\-ñÑáéíóúÁÉÍÓÚ]*)$)",
            lerroa
        ):
            sys.exit("\tERROREA: " + str(lerroZenbakia) +
                     ". lerroak ez du sintaxia betetzen: " + lerroa)

        if lerroa.find("-") == -1:
            sys.exit("\tERROREA: " + str(lerroZenbakia) +
                     ". lerroko hitzak ez du gidoirik: " + lerroa)

        if lerroa in gidoidunHitzak:
            sys.exit("\tERROREA: " + str(lerroZenbakia) +
                     ". lerroko hitza errepikatuta dago fitxategi honetan: " +
                     lerroa)

        if lerroa[0] == '+':
            gidoidunHitzak.append(lerroa[2:len(lerroa)])

print("\tAurkitutako hitzak: " + str(len(gidoidunHitzak)))

inputGidoidunakFitxategia.close()

###############################################################################

print("\nSintaxia egiaztatzen...\t " + inputIzenakBidea)

inputIzenakFitxategia = open(inputIzenakBidea, 'r')

izendunHitzak = list()

lerroZenbakia = 0
while True:
    lerroa = inputIzenakFitxategia.readline()
    lerroZenbakia = lerroZenbakia + 1

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '').strip()

    # Onartzen dira: lerro hutsa, iruzkina, hitza.
    # Lerroko lehen karakterea ezin da hutsunea izan
    # Hitzak letra larriren bat izan behar du
    if lerroa and lerroa[0] != "#":
        if lerroa.find("#") >= 0:
            lerroa = lerroa[
                0:
                lerroa.find("#") - 1
            ]
            lerroa = lerroa.strip()

        if not re.search(
            "(^$|^[\+|\-] (#.*|[a-zA-Z\-ñÑáéíóúÁÉÍÓÚ]*)$)",
            lerroa
        ):
            sys.exit("\tERROREA: " + str(lerroZenbakia) +
                     ". lerroak ez du sintaxia betetzen: " + lerroa)

        if lerroa == lerroa.lower():
            sys.exit("\tERROREA: " + str(lerroZenbakia) +
                     ". lerroko hitzak ez du letra larririk: " + lerroa)

        if lerroa in izendunHitzak:
            sys.exit("\tERROREA: " + str(lerroZenbakia) +
                     ". lerroko hitza errepikatuta dago fitxategi honetan: " +
                     lerroa)

        if lerroa[0] == '+':
            izendunHitzak.append(lerroa[2:len(lerroa)])

print("\tAurkitutako hitzak: " + str(len(izendunHitzak)))

inputIzenakFitxategia.close()

###############################################################################

print("\nSintaxia egiaztatzen...\t " + inputErrimakBidea)

inputErrimakFitxategia = open(inputErrimakBidea, 'r')

gehitzekoErrimak = list()

lerroZenbakia = 0
while True:
    lerroa = inputErrimakFitxategia.readline()
    lerroZenbakia = lerroZenbakia + 1

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '').strip()

    # Onartzen dira: lerro hutsa, iruzkina, hitza eta iruzkina...
    # Lerroko lehen karakterea ezin da hutsunea izan
    if lerroa and lerroa[0] != "#":
        if lerroa.find("#") >= 0:
            lerroa = lerroa[
                0:
                lerroa.find("#") - 1
            ]
            lerroa = lerroa.strip()

        if not re.search(
            "(^$|^(#.*|[\sa-zA-Z\-ñÑáéíóúÁÉÍÓÚ]*(\s*#.*)?)$)",
            lerroa
        ):
            sys.exit("\tERROREA: " + str(lerroZenbakia) +
                     ". lerroak ez du sintaxia betetzen: " + lerroa)

        if lerroa in gehitzekoErrimak:
            sys.exit("\tERROREA: " + str(lerroZenbakia) +
                     ". lerroko hitza errepikatuta dago fitxategi honetan: " +
                     lerroa)

        gehitzekoErrimak.append(lerroa)

print("\tAurkitutako hitzak/errimak: " + str(len(gehitzekoErrimak)))

inputErrimakFitxategia.close()

###############################################################################

gehitzekoErrimak.extend(gidoidunHitzak)
gehitzekoErrimak.extend(izendunHitzak)

print(
    "\nGehitu nahi diren hitzak/errimak guztira: " +
    str(len(gehitzekoErrimak))
)

###############################################################################

print("\n\tErrimategiko errimen zerrenda lortzen...")

errimategikoHitzak = list()

inputErrimategiaFitxategia = open(inputOutputErrimategiaBidea, 'r')

while True:
    lerroa = inputErrimategiaFitxategia.readline()

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    if len(lerroa) == 0:
        continue

    # Lerro osoa iruzkina bada, saltatu lerroa
    if lerroa[0] == "#":
        continue

    # print ("\tLerroa: " + lerroa)

    hitza, estatistikak = lortu_hitza_lerrotik(lerroa)
    errimategikoHitzak.append(hitza["hitza"])

print("\t\tAurkitutako hitzak: " + str(len(errimategikoHitzak)))

inputErrimategiaFitxategia.close()

###############################################################################

print(
    "\n\tErrima berririk dagoen egiaztatzen (dagoeneko Errimategian ez " +
    "daudenak)..."
)

errepikatuGabekoErrimak = gehitzekoErrimak.copy()

if len(errepikatuGabekoErrimak) > 0:
    for errima in errimategikoHitzak:
        if errima in errepikatuGabekoErrimak:
            errepikatuGabekoErrimak.remove(errima)

###############################################################################

if len(errepikatuGabekoErrimak) == 0:
    print("\t\tEz dago errima berririk.")
else:
    print(
        "\t\t" + str(len(errepikatuGabekoErrimak)) +
        " errima berri daude (errepikatu gabeak)."
    )

    ###########################################################################

    print(
        "\nFitxategi baten babeskopia egiten...\n" +
        "\tJatorria: " + inputOutputErrimategiaBidea + "\n" +
        "\tKopia:    " + inputOutputErrimategiaBabeskopiaBidea
    )

    shutil.copy(
        inputOutputErrimategiaBidea,
        inputOutputErrimategiaBabeskopiaBidea)

    ###########################################################################

    print(
        "\nErrima berriak Errimategiaren bukaeran gehitzen...\t" +
        inputOutputErrimategiaBidea
    )

    outputErrimategiaFitxategia = open(inputOutputErrimategiaBidea, 'a')

    outputErrimategiaFitxategia.write("\n\n")

    for errima in errepikatuGabekoErrimak:
        if errima in errimategikoHitzak:
            print(
                "\tABISUA: hitz/errima hau ezikusi da dagoeneko " +
                "Errimategian badagoelako: " +
                errima)
        else:
            outputErrimategiaFitxategia.write("\n" + errima)
            print("\tIdatzi! " + errima)

    print(
        "\n" + str(len(errepikatuGabekoErrimak)) +
        " errima berriak ondo gehitu dira."
    )

###############################################################################

amaituDataOrdua = datetime.datetime.now()
print(
    "\nAMAITU - " + str(amaituDataOrdua) +
    " (" + str(amaituDataOrdua - hasiDataOrdua) + ")"
)
