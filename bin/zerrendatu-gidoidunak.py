#!/usr/bin/env python3

###############################################################################
#
# Script honek esaldi garbietan gidoiak dituzten hitz berriak antzeman eta
# eskuz berrikusi ahal izateko gordetzen ditu. Letra larriren bat bituzten
# hitzak ez ditu kontuan hartzen (horiek izenen script-ak hartzen ditu
# kontuan).
#
# Besteak beste ondokoak egiten ditu:
#   - "gidoidunak" fitxategiak sintaxia betetzen duela ziurtatu.
#   - "esaldiak-garbi" fitxategitik gidoidun hitzak lortu.
#   - "gidoidunak" fitxategia aldatu aurretik bere babeskopia egin.
#   - Aurreko informazioarekin "gidoidunak" fitxategia berregin.
#
# Sarrerak eta irteerak:
#    <-- input/gidoidunak-header
#    <-- input-output/esaldiak-garbi
#    <-> input-output/gidoidunak
#    --> input-output/gidoidunak.back
#    --> (pantaila)
#
###############################################################################

import datetime
import shutil
import os
from lib import (
    lortu_proiektuaren_bidea
)

# Fitxategiak
proiektuarenBidea = lortu_proiektuaren_bidea()
inputEsaldiakBidea = proiektuarenBidea + "/input-output/esaldiak-garbi"
inputGidoidunakHeaderBidea = proiektuarenBidea + "/input/gidoidunak-header"
inputOutputGidoidunakBidea = proiektuarenBidea + "/input-output/gidoidunak"
outputGidoidunakBabeskopiaBidea = inputOutputGidoidunakBidea + ".back"

hasiDataOrdua = datetime.datetime.now()
print("HASI - " + str(hasiDataOrdua))

###############################################################################

egungoGidoidunakZerrenda = list()

print("\nEgungo gidoidun hitzak lortzen (gidoidunak)...")

inputGidoidunakFitxategia = open(inputOutputGidoidunakBidea, 'r')

while True:
    lerroa = inputGidoidunakFitxategia.readline()

    egungoGidoiduna = dict()

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    if lerroa != '' and not lerroa.isspace() and not lerroa.startswith('#') and not lerroa.endswith('-'):
        lerrokoEragiketa = ''
        lerrokoHitza = lerroa
        if len(lerroa) >= 2 and (lerroa[0] == '+' or lerroa[0] == '-'):
            if lerroa[1] != ' ':
                continue
            lerrokoEragiketa = lerroa[0]
            if len(lerroa) <= 2:
                lerrokoHitza = ''
            else:
                lerrokoHitza = lerroa[2:len(lerroa)]
        egungoGidoiduna["eragiketa"] = lerrokoEragiketa
        egungoGidoiduna["hitza"] = lerrokoHitza

        egungoGidoidunakZerrenda.append(egungoGidoiduna)

print(
    "\tAurkitutako gidoidun hitzak: " +
    str(len(egungoGidoidunakZerrenda))
)

inputGidoidunakFitxategia.close()

###############################################################################

esaldietakoGidoidunakZerrenda = list()

print("\nDeskargatutako esaldien gidoidun hitzak lortzen (esaldiak-garbi)...")

inputEsaldiakFitxategia = open(inputEsaldiakBidea, 'r')

while True:
    lerroa = inputEsaldiakFitxategia.readline()

    if not lerroa:
        break

    lerroa = lerroa.replace('\n', '')

    if lerroa.isspace():
        break

    if lerroa.startswith("#"):
        break

    lerrokoHitzak = lerroa.split(' ')
    for hitza in lerrokoHitzak:
        if hitza.find('-') >= 0 and hitza.islower():
            if len(hitza) >= 2 and hitza not in esaldietakoGidoidunakZerrenda:
                esaldietakoGidoidunakZerrenda.append(hitza)

print(
    "\tDeskargatutako gidoidun hitzak (letra larririk ere ez dutenak): " +
    str(len(esaldietakoGidoidunakZerrenda))
)

inputEsaldiakFitxategia.close()

###############################################################################

print("\nGidoidun zerrenda bateratu berria prestatzen...")

# egungoGidoidunakZerrenda konplexuatik (list(dict())) zerrenda sinplea lortu
egungoGidoidunakZerrendaSinplea = list()
for egungoGidoiduna in egungoGidoidunakZerrenda:
    egungoGidoidunakZerrendaSinplea.append(egungoGidoiduna["hitza"])

gidoidunBerriakZerrenda = list()
berriKopurua = 0
for esaldietakoGidoiduna in esaldietakoGidoidunakZerrenda:
    if esaldietakoGidoiduna not in egungoGidoidunakZerrendaSinplea:
        gidoidunBerria = dict()
        gidoidunBerria["eragiketa"] = ''
        gidoidunBerria["hitza"] = esaldietakoGidoiduna

        gidoidunBerriakZerrenda.append(gidoidunBerria)
        berriKopurua += 1

[egungoGidoidunakZerrenda.append(gidoiduna) for gidoiduna in gidoidunBerriakZerrenda]

print(
    "\tGehitutako gidoidun hitz berriak: " +
    str(berriKopurua)
)

###############################################################################

print("\nGidoidun hitzen zerrenda alfabetikoki ordenatzen...")

egungoGidoidunakZerrenda = sorted(
    egungoGidoidunakZerrenda,
    key=lambda gidoiduna: gidoiduna["hitza"].casefold()
)

###############################################################################

print(
    "\nFitxategi baten babeskopia egiten...\n" +
    "\tJatorria: " + inputOutputGidoidunakBidea + "\n" +
    "\tKopia:    " + outputGidoidunakBabeskopiaBidea)

shutil.copy(
    inputOutputGidoidunakBidea,
    outputGidoidunakBabeskopiaBidea)

###############################################################################

print("\nGidoidun zerrenda berria gordetzen (gidoidunak)...")

outputGidoidunakFitxategia = open(inputOutputGidoidunakBidea, 'w')

print("\n\tGoiburua gordetzen...")

inputGidoidunakHeaderFitxategia = open(inputGidoidunakHeaderBidea, 'r')

while True:
    lerroa = inputGidoidunakHeaderFitxategia.readline()

    if not lerroa:
        break

    outputGidoidunakFitxategia.write(lerroa)

inputGidoidunakHeaderFitxategia.close()

print("\nGidoidun hitzak gordetzen...")

for egungoGidoiduna in egungoGidoidunakZerrenda:
    if egungoGidoiduna["eragiketa"] == '':
        lerroa = egungoGidoiduna["hitza"]
    else:
        lerroa = egungoGidoiduna["eragiketa"] + ' ' + egungoGidoiduna["hitza"]
    outputGidoidunakFitxategia.write("\n" + lerroa)
print("\tGidoidun hitzen kopuru eguneratua: " + str(len(egungoGidoidunakZerrenda)) + " (" + str(len(egungoGidoidunakZerrenda)-len(egungoGidoidunakZerrenda)) + " gehiago)")

outputGidoidunakFitxategia.close()

###############################################################################

amaituDataOrdua = datetime.datetime.now()
print(
    "\nAMAITU - " + str(amaituDataOrdua) +
    " (" + str(amaituDataOrdua - hasiDataOrdua) + ")"
)
