# Nola erabili

Dokumentu honetan errimategia erabiltzen laguntzeko azalpen batzuk biltzen dira.

Ondoko erabilerak aurreikusten dira egin nahi duzunaren arabera:

1. [Errimategiari ikusi nahi dut](https://gitlab.com/bertsotan/errimategia/-/blob/main/ERABILERA.md#1-errimategiari-ikusi-nahi-dut)
2. [Errimategia kopiatu nahi dut nire proiektuetan erabiltzeko](https://gitlab.com/bertsotan/errimategia/-/blob/main/ERABILERA.md#2-errimategia-kopiatu-nahi-dut-nire-proiektuetan-erabiltzeko)
3. [Jabetza publikoan dagoen testu batekin errimategia aberastu nahi dut](https://gitlab.com/bertsotan/errimategia/-/blob/main/ERABILERA.md#3-jabetza-publikoan-dagoen-testu-batekin-errimategia-aberastu-nahi-dut)
4. [Hitz-sorta bat gehitu nahi dut errimategian](https://gitlab.com/bertsotan/errimategia/-/blob/main/ERABILERA.md#4-hitz-sorta-bat-gehitu-nahi-dut-errimategian)
5. [Errima solte batzuk gehitu edo aldatu nahi ditut](https://gitlab.com/bertsotan/errimategia/-/blob/main/ERABILERA.md#5-errima-solte-batzuk-gehitu-edo-aldatu-nahi-ditut)

## 1. Errimategiari ikusi nahi dut

`errimategia` fitxategiak errimategiaren azken bertsioa du eta bertan edukiak ikus ditzakezu formatu ulergarri batean. `errimategia-info` fitxategiak estatistika txiki batzuk ditu.

## 2. Errimategia kopiatu nahi dut nire proiektuetan erabiltzeko

`errimategia.json` fitxategiak errimategiaren azken bertsio du programazio lengoaia gehienek ulertzen duten formatuan. Bertsotan Android aplikazioak fitxategi hau darabil.

*OHARRA: ondorengo erabilerentzat IKT ezagutzak gomendatzen dira. Zure kasua hori ez bada, errimategian ekarpenenak egiteko jar zaitez gurekin [harremanetan](https://gitlab.com/bertsotan/errimategia#harremanetarako) mesedez.*

## 3. Jabetza publikoan dagoen testu batekin errimategia aberastu nahi dut

CC0 lizentzia edo [jabetza publikoa](https://eu.wikipedia.org/wiki/Jabari_publiko) zer den guztiz ziur ez bazaude, aurrera egin baino lehen jar zaitez gurekin [harremanetan](https://gitlab.com/bertsotan/errimategia#harremanetarako). Errimategian [egile-eskubideak](https://eu.wikipedia.org/wiki/Egile-eskubideak) dituzten edukiak saihestu nahi ditugu!

Gehitu nahi dituzun edukiak esaldiak badira, testua `esaldiak` fitxategian itsatsi (lerro bakoitzean esaldi garbi bat): Adibidez:

    Bazen behin Aitor izeneko artzain gazte bat.
    Ene!
    Nor da galtzaurdina?
    Beraz, aulki-mota bakoitzari bere on-line kodea dagokio:
    Horregatik Gasteizen bertso-eskolan izena ematea erabaki nuen.

**`zatitu-esaldiak.py`** scripta exekutatu eta errorerik balego `esaldiak` fitxategia zuzendu. Ondoko datu fitxategiak irakurri eta idazten ditu:

    - (<--) input/esaldiak
    - (-->) input-output/gidoidunak
    - (-->) input-output/izenak
    - (-->) input-output/errimak

Esaldietan aurkitutako gidoidun hitz guztiak `gidoidunak` fitxategian gordetzen dira, baina hurrengo urratsetan ezikusiak izan daitezen iruzkinduta (aurretik # ikurra jarrita):

    #aulki-mota
    #on-line
    #bertso-eskolan

Esaldietan aurkitutako letra larridun hitz guztiak `izenak` fitxategian gordetzen dira, baina hurrengo urratsetan ezikusiak izan daitezen iruzkinduta (aurretik # ikurra jarrita):

    #Bazen
    #Aitor
    #Ene
    #Nor
    #Beraz
    #Horregatik
    #Gasteizen

`errimak` fitxategian gainontzeko hitzak gordetzen dira, hasiera batean errimategirako interesgarriak izateko aukera gehien dituztenak:

    behin
    izeneko
    artzain
    gazte
    bat
    da
    galtzaurdina
    bakoitzari
    bere
    dagokio
    izena
    ematea
    erabaki
    nuen

## 4. Hitz-sorta bat gehitu nahi dut errimategian

Gehitu nahi dituzun edukiak hitz edo errima solteak badira, `gidoidunak` eta `izenak` fitxategiak hustu eta gehitu nahi dituzun hitz guztiak `errimak` fitxategian itsatsi (lerro bakoitzean errima bat). Adibidez:

    zati
    basati
    irrati
    setati
    bati
    adina
    ekinez egina
    sardina
    shoppinga
    algara
    al gara
    bait gara
    gola
    hala-nola
    traola
    Coca-Cola
    Guardiola

Aurreko urratsetik bazatoz, aipatutako hiru fitxategien edukiak berrikusi:

- `gidoidunak` fitxategian berreskuratzea merezi dutela uste duzun hitzetan # hizkia kendu. Adibidez: *"#bertso-eskolan"* --> *"bertso-eskolan"*.
- `izenak` fitxategian berreskuratzea merezi dutela uste duzun hitzetan # hizkia kendu. Adibidez: *"#Gasteizen"* --> *"Gasteizen"*.
- `errimak` fitxategian egokiak ez diren errimak ezabatu edo # hizkia erabiliz iruzkin bihurtu (edo zuzenean ezabatu). Adibidez: *"galtzaurdina"* --> *"#galtzaurdina"*.

Fitxategiok prestatutakoan **`gehitu-errimak.py`** scripta exekutatu. `errimategia` fitxategiaren bukaeran errima berriak gehituko dira. Iruzkindutako lerro guztiak (# hizkiarekin hasten direnak) ezikusi egingo dira. Dagoeneko erriategian dauden hitzak ez dira berriz gehituko errepikapenak saihesteko.

Scriptak ondoko datu fitxategiak irakurri eta idazten ditu:

    - (<--) input-output/gidoidunak
    - (<--) input-output/izenak
    - (<--) input-output/errimak
    - (<->) input-output/errimategia

## 5. Errima solte batzuk gehitu edo aldatu nahi ditut

Aurreko urratsak eman ondoren edo beste edozein unetan, `errimategia` fitxategia editatu dezakezu. Errimak gehitu, kendu eta moldatu ditzakezu. Adi: irakurri fitxategi honen goiburuko azalpenak sintaxi zuzena zehatz-mehatz jarraitzeko. Errima bakoitzari ahoskera, erabilgarritasun maila, markak eta gaiak zehaztu diezaiekezu. Zenbat eta informazio osagarri gehiago erantsi, hobeto. Adibidez:

    zati
    basati
    irrati {Hedabideak}
    setati
    bati {Zenbakiak}
    adina (pol)
    ekinez egina
    sardina {Itsasoa} {Animaliak}
    shoppinga [xopina] (erd)
    algara
    al gara
    bait gara [baikara] #bait eta ez partikula aurrean dagoenean ahoskera aldatzen da
    gola {Kirola}
    hala-nola
    traola 3 (pol) {Itsasoa} {Teknologia}
    Coca-Cola {Janaria}
    Guardiola {Kirola}

Moldaketak amaitutakoan, **`berregin-errimategia.py`** scripta exekutatu eta errorerik balego `errimategia` fitxategia zuzendu. Fitxategi honetako atal guztiak ezabatu eta automatikoki berriz sortzen dira: goiburua, gaien zerrenda, behar diren bukaerak eta abar. Errima guztiak behar duten ataletan kokatzen dira eta dagozkien ordenan. Errimen lerro berean gehitzen dituzun iruzkinak mantendu egiten dira fitxategia berregitean. Lerro hasieran ez jarri iruzkinik, errimategia berregitean galdu egingo baitira.

Scriptak ondoko datu fitxategiak irakurri eta idazten ditu:

    - (<--) input/errimategia-header
    - (<->) input-output/errimategia

Azkenik, errimategiaren bertsio berri bat sortu nahi duzunean **`esportatu-errimategia.py`** scripta exekutatu. Ondoko datu fitxategiak irakurri eta idazten ditu:

    - (<--) input-output/errimategia
    - (-->) output/errimategia.json
    - (-->) output/errimategia-info
