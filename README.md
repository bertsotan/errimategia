# Bertsotan Errimategia

Bertsotarako errimen bilduma edo "errimategi" hau [**Bertsotan**](https://gitlab.com/bertsotan) proiektuaren parte da. Errimategi honen helburua Bertsotan-ek proposatzen dituen errimak biltzea, zehaztea eta prestatzea da. Bi informazio iturri ditu:

1. Common Voice-ko idatzizko esaldien hitzak. [Mozilla Common Voice](https://commonvoice.mozilla.org/eu/about) proiektuak bildutako euskarazko esaldietatik hainbat hitz proiektu honetan erabiltzen dira (31.000 hitz handik datoz). Hitzok euskara batuan daude, egoera eta gai desberdinak jorratzen dituzte eta domeinu publikoan daude. Beraz, proiektu honetarako egokiak dira.
2. Errima propioak. Bertsotan proiekturako espresuki bildutako hitzak, izenak, esapideak eta abar. Aurreko hitzak osatzen dituzte, elkarrekin Errimategi aberats bat lortuz.

Errimategiak une honetan zenbat errima dituen eta beste [zenbait estatistika](https://gitlab.com/bertsotan/errimategia/-/blob/main/output/errimategia-info) kontsulta ditzakegu.

## Ezaugarriak

Errima bakoitzari buruz zenbait datu gordetzen dira. Informazio honek errima egokiak proposatzea ahalbidetzen du, baita proposatutakoak bistaratzeko unean bertsolariari informazio gehigarria eskaintzea ere. Errima bakoitzak gehienez izan dezakeen informazioa ondokoa da:

- Errimaren hitza (edo kasu batzuetan hitzak): *"hegoa"*, *"izanda"*, *"izan da"*, *"ez gara"*, *"gai"*, *"gay"*, *"Richard Gere"*.
- Errima nola ahoskatzen den: *"[egoa]"*, *"[izanda]"*, *"[ezkara]"*, "*[gai]*". Maileguetan eta erdarazko izenetan bereziki erabilgarria da: *"[gei]"*, *"[ritxarger]"*.
- Errimaren gutxi gorabeherako erabilgarritasun maila. Arlo zehatzetako lexikoa egoera gehienetan ez da oso erabilgarria izaten: *"neutroiak"*, *"protista"*, *"eskritura"*, *"taylorismoa"*. Postposizio-lokuzioak adibidez erabilgarriagoak izan ohi dira: *"bezala"*, *"ostera"*, *"aurretik"*.
- Markak. Ondokoak bereizten dira:
  - Esanahi bat baino gehiago duen hitz polisemikoa (*"hori"* erakuslea/kolorea, *"katu"* animalia/tresna) edo berdin idazten diren hitz homografoak (*"ehun"* zenbakia/oihala, *"tutu"* hodia/gona).
  - Hitz lokala; euskalki, herri edo zonalde batzuetan erabiltzen dena: *"deko"*, *"zozten"*, *"maleroski"*, *"amoros"*.
  - Erderakada, estranjerismoa, neologismoa eta hiztegian agertu ez arren kalean erabiltzen den hitza: *"parida"*, *"marchandisinga"*, *"reggaetoia"*.
  - Lagunarteko hitza; informala, arrunta. Hiztegietan onartuta ez dagoen baina kalean erabiltzen den hitza ere izan daiteke: *"makilajea"*, *"ligoi"*, *"keba"*, *"flato"*, *"rayatu"*.
- Gaiak. Besteak beste ondokoak bereizten dira: Musika, Literatura, Itsasoa, Gabonak, Kirola, Ogibideak...
- Iruzkinak.

## Nola erabili

Errimategi hau erabiltzeko azalpenak [ERABILERA](https://gitlab.com/bertsotan/errimategia/-/blob/main/ERABILERA.md) dokumentuan daude.

## Ekarpenak

Proiektu honek izaera parte-hartzailea du eta ekarpenak ongi etorriak dira.

Mesedez, iturburu-kodean egin nahi dituzun ekarpenak Git proiektu honetantxe oinarritu: https://gitlab.com/bertsotan/errimategia/ Ondoren, egin *Merge Request* bat proposatu nahi dituzun aldaketak azalduz.

Android aplikazio hau **Python** programazio lengoaian garatuta dago. Programatzeko hauek erabil ditzakezu:

- Python 3
- Visual Studio Code
- Git

## Lizentziak

Proiektu hau [software librea](https://eu.wikipedia.org/wiki/Software_libre) da:

- Iturburu-kodea GNUren Affero Lizentzia Publiko Orokorraren 3. bertsiopean dago ([AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.txt) lizentzia).
- Testuak eta **errimen bilduma jabetza publikoan** daude ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.eu) eskaintza). Beraz, askea zara datuak nahi duzun lekuan eta nahi duzun moduan erabiltzeko, jatorria aipatu beharrik gabe. Hala ere, aipamenen bat egitea edo abisuren bat jasotzea eskertzekoa da, hemen egindako lana zabaldua eta erabilia izatea beti baita pozgarria.

## Harremanetarako

Errimategian ekarpen edo proposamenen bat egin nahi baduzu, mesedez sortu ezazu [intzidentzia](https://gitlab.com/bertsotan/errimategia/-/issues) berri bat. Bestelako kontuetarako [bertsotan@ikusimakusi.eus](mailto:bertsotan@ikusimakusi.eus) helbidera idatz dezakezu.
